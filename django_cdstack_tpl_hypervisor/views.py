from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
)
from django_cdstack_tpl_centos_7.django_cdstack_tpl_centos_7.views import (
    handle as handle_centos_7,
)
from django_cdstack_tpl_kvm_common.django_cdstack_tpl_kvm_common.views import (
    handle as handle_kvm_common,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_hypervisor/django_cdstack_tpl_hypervisor"

    template_opts["ceph_cluster_list"] = list()

    for key in template_opts.keys():
        if key.startswith("ceph_cluster_") and key.endswith("_fsid"):
            key_ident = key[:-5]

            ceph_opts = dict()
            ceph_opts["ceph_internal_id"] = key_ident
            ceph_opts["ceph_fsid"] = template_opts[key_ident + "_fsid"]
            ceph_opts["ceph_public_network"] = template_opts[
                key_ident + "_public_network"
            ]
            ceph_opts["ceph_rbd_default_data_pool"] = template_opts[
                key_ident + "_rbd_default_data_pool"
            ]
            ceph_opts["ceph_secret_uuid"] = template_opts[key_ident + "_secret_uuid"]
            ceph_opts["ceph_cephx_user"] = template_opts[key_ident + "_cephx_user"]
            ceph_opts["ceph_cephx_key"] = template_opts[key_ident + "_cephx_key"]

            template_opts["ceph_cluster_list"].append(ceph_opts)
            ceph_opts[key_ident + "_mon_hostname_list"] = list()
            ceph_opts[key_ident + "_mon_ip_list"] = list()

            for temp_key in template_opts.keys():
                if temp_key.startswith(key_ident + "_mon_") and temp_key.endswith(
                    "_hostname"
                ):
                    mon_key = temp_key[:-9]
                    ceph_opts[key_ident + "_mon_hostname_list"].append(
                        template_opts[mon_key + "_hostname"]
                    )
                    ceph_opts[key_ident + "_mon_ip_list"].append(
                        template_opts[mon_key + "_ip"]
                    )

            ceph_opts["ceph_mon_hostname_list_str"] = ", ".join(
                ceph_opts[key_ident + "_mon_hostname_list"]
            )
            ceph_opts["ceph_mon_ip_list_str"] = ",".join(
                ceph_opts[key_ident + "_mon_ip_list"]
            )

            chained_opts = {**template_opts, **ceph_opts}

            config_template_file = open(
                module_prefix + "/templates/config-fs/dynamic/etc/ceph/ceph.conf", "r"
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                "etc/ceph/" + key_ident + ".conf",
                config_template.render(chained_opts),
            )

            config_template_file = open(
                module_prefix + "/templates/config-fs/dynamic/etc/ceph/secret.xml", "r"
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                "etc/ceph/" + key_ident + "-secret.xml",
                config_template.render(chained_opts),
            )

            config_template_file = open(
                module_prefix + "/templates/config-fs/dynamic/etc/ceph/ceph-secret.key",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                "etc/ceph/" + key_ident + "-" + ceph_opts["ceph_cephx_user"] + ".key",
                config_template.render(chained_opts),
            )

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_kvm_common(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    handle_centos_7(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
